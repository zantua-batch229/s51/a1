import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';


export default function Login(){

	const [userName, setUserName] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

		if(userName !== '' && password !== ''){
			setIsActive(true);
		}


	})

	function Login(){
		setUserName('');
		setPassword('');
		setIsActive(false);
		alert(`Welcome ${userName}!`);
	}

	return(
			<Form onSubmit={(e) => Login(e)}>

				<Form.Group className="mb-3" id="userName">
					<Form.Label>User Name</Form.Label>
					{/*Form control is self-closing. It is where the values are set*/}
					<Form.Control type="email" placeholder="Email" required value={userName} onChange={e => setUserName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" id="password">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Password" required value={password} onChange={e => setPassword(e.target.value)}/>
				</Form.Group>

				
				{
					isActive 
					?
					<Button variant="success" type="submit" id="loginBtn">
		        	Login
		      		</Button>
		      		:
		      		<Button variant="success" type="submit" id="loginBtn" disabled>
		        	Login
		      		</Button>

	      		}
	      		

			</Form>



		)
}
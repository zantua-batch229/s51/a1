import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register() {

	// State Hooks -> store values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive]= useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		// Validation to enable Register button when all fields are populated and both passwords match

		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
	})

	// Function to simulate user registration

	function registerUser(e){
		// prevents page redirection via form submission
		e.preventDefault();
		setEmail('');
		setPassword1('');
		setPassword2('');
		alert("Thank you for registering!");
	}

	return (

		<Form onSubmit={(e) => registerUser(e)}>
	      <Form.Group className="mb-3" id="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" id="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
	      </Form.Group>

	      <Form.Group className="mb-3" id="password2">
	        <Form.Label>Re-Enter Password</Form.Label>
	        <Form.Control type="password" placeholder="Re-Enter Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
	      </Form.Group>


	      
	      {
	      	isActive 
	      	? 
	      	<Button variant="primary" type="submit" id="submitBtn">
	        	Register
	      	</Button> 
	      	:
	      	<Button variant="primary" type="submit" id="submitBtn" disabled>
	        	Register
	      	</Button>
	      }

	      
    	</Form>
	)
}
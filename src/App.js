// To start the app type npom start on gitbash on main folder
import './App.css';
import AppNavbar from './components/AppNavbar.js'
import {Container} from 'react-bootstrap/';
//import Home from './pages/Home.js';

//import Courses from  './pages/Courses.js'
import Login from  './pages/Login.js'
import Register from  './pages/Register.js'


function App() {
  return (
    //we are mounting our components to prepare for output for rendering
    //para di magagawan ng space ang components kailangan ng parent
    //Fragment "<> and </> to act as parents of components"
    <>
      <AppNavbar/>
      <Container>
      {/*<Home/>
        <Courses/>*/}
        <Login/>
        <Register/>
      </Container>
    </>
  );
}

export default App;
